<?php

namespace App\Http\Controllers;

use App\Report;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    public function index()
    {
        $reports = auth()->user()->reports;
        return view('reports', compact('reports'));
    }


    public function create (Request $request, Report $report) {
       $this->authorize('create', $report);
       
        return view('reports.create');
    }


    public function store(Request $request) {
         $request->validate([
            'crime' => 'required',
            'description'  =>   'required',
            'date'  =>   'required',
            'means_of_communication'  =>   'required',
            'exact_location'  =>   'required',
            'nearest_police_station'  =>   'required',
            'nearest_public_primary_school'  =>   'required',
            'phone_number_of_trusted_person'  =>   'required',
        ]);

        Report::create($request->all());

        return redirect()->route('reports.index')
                            ->with('success','Report made successfully.');
                            
    }

    public function show(Report $report)
    {
        return view('reports.show',compact('report'));
    }

    public function edit(Report $request)
    {
        return view('reports.edit',compact('report'));
    }

    public function update(Request $request, Report $report)
    {
        $request->validate([
            'crime' => 'required',
            'description'  =>   'required',
            'date'  =>   'required',
            'means_of_communication'  =>   'required',
            'exact_location'  =>   'required',
            'nearest_police_station'  =>   'required',
            'nearest_public_primary_school'  =>   'required',
            'phone_number_of_trusted_person'  =>   'required',
        ]);

        Report::update($request->all());

        return redirect()->route('reports.index')
                            ->with('success','Report updated successfully.');
    }

    public function destroy(Report $report)
    {
        $report->delete();

        return redirect()->route('reports.index')
                        ->with('success','Report deleted successfully');
    }

}