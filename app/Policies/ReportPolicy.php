<?php

namespace App\Policies;

use App\Report;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ReportPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function view(User $user, Report $report)
        {
            return $user->id=== $report->user_id;
        }

    public function delete(User $user, Report $report)
        {
            return $user->id=== $report->user_id;
        }
    
    public function edit(User $user, Report $report)
        {
            return $user->id=== $report->user_id;
        }
}
