<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    protected $fillable = [
        'crime',
        'description',
        'date',
        'means_of_communication',
        'exact_location',
        'nearest_police_station',
        'nearest_public_primary_school',
        'phone_number_of_trusted_person',
        'user_id'
    ];

    public function user()
    {
        return $this->belongsTo(App\User::class);
    }
}
