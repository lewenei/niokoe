<!DOCTYPE html>
<html lang="en">

@include('admin.head')

<body>
  <!-- container section start -->
  <section id="container" class="">


    @include('layouts.header')
    <!--header end-->

    <!--sidebar start-->
    <aside>
      <div id="sidebar" class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu">
          <li class="active">
          <a class="" href="{{url('/') }}">
                          <i class="icon_house_alt"></i>
                          <span>Dashboard</span>
                      </a>
          </li>
          <li class="active">
            <a class="" href="{{url('/contact') }}">
                          <i class="icon_phone"></i>
                          <span>Contact Us</span>
                      </a>
          </li>
          <li class="active">
            <a class="" href="{{url('/reports') }}">
                          <i class="icon_comment"></i>
                          <span>Reports</span>
                      </a>
          </li>
        </ul>
        <!-- sidebar menu end-->
      </div>
    </aside>
    <!--sidebar end-->

    <!--main content start-->
    <section id="main-content">
      <section class="wrapper">
        <div class="row">
          <div class="col-lg-12">
            <h3 class="page-header"><i class="fa fa-files-o"></i> Report a Case </h3>
          </div>
        </div>
        <!-- Form validations -->
        <div class="row">
          <div class="col-lg-12">
            <section class="panel">
              <header class="panel-heading">
                Fill in Form to Report a Case of Gender Based Violence
              </header>
              <div class="panel-body">
                <div class="form ">
                  

                          @if ($errors->any())
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

                  <form class="form-validate form-horizontal" id="feedback_form" method="POST" action="{{ route('reports.store') }}">
                    @csrf
                    <div class="form-group">
                      <label class="control-label col-lg-2">Crime</label>
                      <div class="col-lg-10">
                        <select class="form-control" name="crime">
                                                <option value="">- Select Crime -</option>
                                                <option value="rape">Rape</option>
                                                <option value="Sexual Assault">Sexual Assault</option>
                                                <option value="child marriage">Child Marriage</option>
                                                <option value="fgm">Female Genital Mutilation</option>
                                                <option value="sex trafficking">Trafficking for Sex or Slavery</option>
                                              </select>
                      </div>
                    </div>
                    <div class="form-group ">
                      <label for="ccomment" class="control-label col-lg-2">Description</label>
                      <div class="col-lg-10">
                        <textarea class="form-control " id="comment" name="description" required></textarea>
                      </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-2">Date</label>
                        <div class="col-lg-10">
                          <input id="dp1" type="text" name="date" value="01-10-2020" size="16" class="date form-control">
                        </div>
                      </div>
                    <div class="form-group ">
                      <label for="curl" class="control-label col-lg-2">Preferred Means of Communication</label>
                      <div class="col-lg-10">
                        <select class="form-control" name="means_of_communication">
                          <option value="">- Select Preferred Mean-</option>
                          <option value="Phone">Phone Call</option>
                          <option value="SMS">SMS</option>
                          <option value="mail">e-Mail</option>
                          <option value="whatsapp">WhatsApp</option>
                        </select>
                      </div>
                    </div>
                    <div class="form-group ">
                      <label for="cname" class="control-label col-lg-2">What is Your Exact Location <span class="required">*</span></label>
                      <div class="col-lg-10">
                        <input class="form-control" id="subject" name="exact_location" minlength="5" type="text" required />
                      </div>
                    </div>
                    <div class="form-group ">
                      <label for="ccomment" class="control-label col-lg-2">Nearest Police Station</label>
                      <div class="col-lg-10">
                        <textarea class="form-control " id="ccomment" name="nearest_police_station" required></textarea>
                      </div>
                    </div>
                    <div class="form-group ">
                      <label for="cname" class="control-label col-lg-2">Nearest Public Primary School <span class="required">*</span></label>
                      <div class="col-lg-10">
                        <input class="form-control" id="cname" name="nearest_public_primary_school" minlength="5" type="text" required />
                      </div>
                    </div>
                 <div>
                  <input type="hidden" id="user_id" name="user_id" value="{{ Auth::id() }}"/>
                   </div>   
                    <div class="form-group ">
                      <label for="cname" class="control-label col-lg-2">Phone Number of Someone you Trust <span class="required">*</span></label>
                      <div class="col-lg-10">
                        <input class="form-control" id="cname" name="phone_number_of_trusted_person" minlength="5" type="text" required />
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-lg-offset-2 col-lg-10">
                        <button class="btn btn-primary" type="submit">Submit</button>
                      </div>
                    </div>
                  </form>
                </div>

              </div>
            </section>
          </div>
        </div>
        <div class="row">
          
        <!-- page end-->
      </section>
    </section>



    <!--main content end-->
  </section>
  <!-- container section start -->

  <!-- javascripts -->
  @include('admin.script')

  <script type="text/javascript">

    $('.date').datepicker({  

       format: 'mm-dd-yyyy'

     });  

</script> 

</body>

</html>
