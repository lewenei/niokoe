<!DOCTYPE html>
<html lang="en">

@include('admin.head')

<body>
  <!-- container section start -->
  <section id="container" class="">


    @include('layouts.header')
    <!--header end-->

    <!--sidebar start-->
    <aside>
      <div id="sidebar" class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu">
          <li class="active">
          <a class="" href="{{url('/') }}">
                          <i class="icon_house_alt"></i>
                          <span>Dashboard</span>
                      </a>
          </li>
          <li class="active">
            <a class="" href="{{url('/contact') }}">
                          <i class="icon_phone"></i>
                          <span>Contact Us</span>
                      </a>
          </li>
        </ul>
        <!-- sidebar menu end-->
      </div>
    </aside>
    <!--sidebar end-->

    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
          <div class="row">
            <div class="col-lg-12">
              <h3 class="page-header"><i class="fa fa fa-comment"></i> Reports</h3>
              
            </div>
          </div>
          <!-- page start-->
          <div class="row">
            <div class="col-lg-12">
              <section class="panel">
                <header class="panel-heading">
                  These are your Reports
                </header>
  
                <table class="table table-striped table-advance table-hover">
                  <tbody>
                    <tr>
                      <th><i class="icon_profile"></i> Crime</th>
                      <th><i class="icon_calendar"></i> Description</th>
                      <th><i class="icon_mail_alt"></i> Date</th>
                      <th><i class="icon_pin_alt"></i> Means of Communication</th>
                      <th><i class="icon_mobile"></i> Location</th>
                      <th><i class="icon_cogs"></i> Phone Number</th>
                    </tr>
                    @foreach ($reports as $report)
                        
                   
                    <tr>
                    <td>{{ $report->crime }}</td>
                      <td>{{ $report->description }}</td>
                      <td>{{ $report->date }}</td>
                      <td>{{ $report->means_of_communication }}</td>
                      <td>{{ $report->exact_location }}</td>
                      <td>{{ $report->phone_number_of_trusted_person }}</td>
                      <td>
                        <div class="btn-group">
                          <a class="btn btn-primary" href="{{ route('reports.edit',$report->id)}}">Edit</a>
                        <a class="btn btn-success" href="{{ route('reports.show',$report->id)}}">Show</a>
                        @csrf
                        @method('DELETE')
                        @can('delete', $report)
                        <a class="btn btn-danger" delete-id="{{$report->id}}" type="submit">Delete</a>
                        @endcan
                        </div>
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
                
                
              </section>
            </div>
          </div>
          <!-- page end-->
        </section>
      </section>
      <!--main content end-->
      <div class="text-right">
        <div class="credits">
            <!--
              All the links in the footer should remain intact.
              You can delete the links only if you purchased the pro version.
              Licensing information: https://bootstrapmade.com/license/
              Purchase the pro version form: https://bootstrapmade.com/buy/?theme=NiceAdmin
            -->
            Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
          </div>
      </div>
    </section>
    <!-- container section end -->
    <!-- javascripts -->
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <!-- nicescroll -->
    <script src="js/jquery.scrollTo.min.js"></script>
    <script src="js/jquery.nicescroll.js" type="text/javascript"></script>
    <!--custome script for all page-->
    <script src="js/scripts.js"></script>
  
  
  </body>
  
  </html>
  